#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pos_tagger import pos_tag
from versionnonltk import launch, addCategoryWithTextFile, launchMultipleCategory
from flask import Flask, render_template, request, flash, redirect, url_for
import os
from werkzeug.utils import secure_filename

app = Flask(__name__)

UPLOAD_FOLDER = './public/upload'
ALLOWED_EXTENSIONS = {'txt'}

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def homepage():
    return render_template("home.html")


@app.route('/about')
def about():
    return render_template("about.html")


@app.route('/parser', methods=['GET', 'POST'])
@app.route('/parser/<name>', methods=['GET', 'POST'])
def parser(name=''):
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            filename_noext = filename.split('.')[0]
            file_content = open(os.path.join(app.config['UPLOAD_FOLDER'], filename), 'r').read()
            addCategoryWithTextFile(filename_noext, file_content)
            return redirect(url_for('parser')+'/'+filename)
    if name == '':
        parsed = []
    else:
        path = os.path.join(app.config['UPLOAD_FOLDER'], name)
        file = open(path, 'r')
        parsed = pos_tag(file.read())
    return render_template("parser.html", parsed=parsed)


@app.route('/gen/', methods=['GET', 'POST'])
@app.route('/gen/<n>/<cat>/')
@app.route('/gen/<n>/<cat>/<length>/')
@app.route('/gen/<n>/<cat>/<length>/<number>')
def generator(n=3, cat='culture', length=10, number=5):
    sentences = []
    categories = []
    for data_name in os.listdir('alldatas'):
        if 'ngrams' not in data_name:
            categories.append(data_name.split('.')[0])
    if len(cat.split('+')) > 1:
        for i in range(int(number)):
            sentences.append(launchMultipleCategory(cat.split('+'), n, int(length)))
    else:
        for i in range(int(number)):
            sentences.append(launch(cat, n, int(length)))
    return render_template("generator.html", category=cat, length=length, number=number,
                           sentences=sentences, categories=categories)

