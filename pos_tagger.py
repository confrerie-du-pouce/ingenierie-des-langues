import spacy

nlp = spacy.load('fr_core_news_md')


def pos_tag(text: str):
    doc = nlp(text)
    return [[token.text, token.pos_] for token in doc]
