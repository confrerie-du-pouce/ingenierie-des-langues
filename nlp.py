import spacy
import os
from nltk import ngrams

nlp = spacy.load('fr_core_news_md')


def get_tokens_object(string: str):
    """
    Use spaCy to return list of Token.
    :param string: the text to tokenize
    :return: list of tokens
    """
    doc = nlp(string.strip())
    return [token for token in doc]


def get_lemmas(tokens: list):
    """
    Extract list of lemmas from Token's list.
    :param tokens: list of Token object (from spaCy).
    :return: list of string
    """
    return [token.lemma_ for token in tokens]


def get_tokens(tokens: list):
    """
    Extract list of token from Token's list.
    :param tokens: list of Token object (from spaCy).
    :return: list of string
    """
    return [token.text for token in tokens]


def get_ngrams(words: list, n: int):
    """
    Generate n-grams based on wods' list.
    :param words: list of string
    :param n: number of word grams are based
    :return: list of n-grams
    """
    return [' '.join(gram) for gram in ngrams(words, n)]


def get_pos_tag(tokens: list):
    """
    Get a dictionnary that associate a token's word with its POS tag.
    :param tokens: list of Token (spaCy object)
    :return: dict with word as key and POS tag as value
    """
    return [[token.text, token.pos_] for token in tokens]


def ngrams_frequencies(ngrams: list):
    freq = {}
    for ngram in set(ngrams):
        freq[ngram] = ngrams.count(ngram)/len(ngrams)
    return freq


def count_ngrams(ngrams: list):
    unique_ngrams = 0
    for ngram in set(ngrams):
        if ngrams.count(ngram) == 1:
            unique_ngrams += 1
    return [len(ngrams), unique_ngrams]


def read_file(path: str, encoding='utf-8'):
    return open(path, 'r', encoding=encoding).read()


def read_directory(path: str):
    texts = []
    for file_path in os.listdir(path):
        texts.append(read_file(file_path))
    return nlp.pipe(texts)


def compute_directory(path: str):
    docs = read_directory(path)
