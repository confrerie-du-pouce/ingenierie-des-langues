
                Bruno Le Maire, le ministre de l'Économie, avait plaidé pour un plafond à 200 euros par an pour les personnes les plus modestes.
                
                    Le ministre de l'Economie et des Finances Bruno Le Maire a annoncé ce lundi un accord avec le secteur bancaire français, qui s'est engagé à plafonner à 200 euros par an les frais d'incident pour les publics fragiles.
                
                
                    "Pour les personnes les plus fragiles, celles qui ont accès à cette offre (spécifique), les frais d'incident bancaire ne pourront plus dépasser 20 euros par mois et 200 euros par an", a déclaré Bruno Le Maire, à l'issue d'une rencontre avec les représentants de la Fédération bancaire française.
                
                
                    Des e,engagements vérifiés
                    Les banques ont aussi promis de promouvoir davantage l'offre bancaire "spécifique", réservée aux clients en difficulté financière, dont le nombre de bénéficiaires doit progresser de 30% en 2019, soit 130 000 personnes en plus, a annoncé le ministre. Ces engagements seront "vérifiés" (...) et "si jamais nous ne devions pas parvenir aux résultats que j'ai indiqué, nous en tirerions les conséquences législatives", a toutefois mis en garde Bruno Le Maire.
                
                
                    Créée en 2014 sous l'impulsion du législateur, "l'offre spécifique" doit être proposée par toutes les banques aux personnes en situation de fragilité financière. Elle prévoit un ensemble de services bancaires de base (tenue de compte bancaire, carte de paiement et de retrait, possibilité d'effectuer des virements et des prélèvements) au coût modéré de 3 euros maximum par mois.
                
                
                    Mais quatre ans après son lancement, cette offre ne profite en fin de compte qu'à 375 000 clients bancaires fragiles, soit à peine plus de 10% des personnes éligibles. "C'est trop peu", avait jugé dimanche Bruno Le Maire dans un entretien télévisé.
                
            