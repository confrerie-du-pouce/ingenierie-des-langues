
                Il n'y a qu'une seule stratégie pertinente : continuer de libérer la production et le travail de tous les freins qui les entravent.
                
                    La politique menée par la majorité ne donne pour l'heure pas de résultats. La croissance atteindra 1,7 % cette année et 1,5 % l'année prochaine, ce qui empêche toute augmentation perceptible du pouvoir d'achat. Les créations d'emplois dans le secteur privé ralentissent, le taux de chômage reste aux alentours de 9 %. Le déficit public demeure supérieur à 2,5 % du PIB et continue d'alimenter notre dette souveraine.
                
                
                    Trois explications peuvent expliquer ces chiffres. La première est celle que donne la majorité : le gouvernement opère les réformes adéquates, mais elles mettent du temps à produire leurs effets, d'autant que la conjoncture internationale se dégrade. Certains évoquent une "courbe en J" : à court terme, les réformes peuvent avoir un effet dépressif sur l'économie.
                
                
                    Les dégâts d'une politique fiscale baroque
                    Ce raisonnement n'est pas faux en soi, mais il ne correspond pas à la situation de notre pays. On voit mal quelles réformes d'ampleur auraient pu déprimer l'économie, à part, à la marge, la diminution du nombre d'emplois aidés. Et encore : la dépense publique globale continue d'augmenter, ce qui, au passage, finit de couvrir de ridicule ceux qui parlent d'une "politique d'austérité" qui n'a jamais existé que dans leur cerveau imperméable à la réalité. S'il y a une courbe en J, elle a été provoquée par une politique fiscale baroque.
                
                
                    La CSG a brutalement augmenté au premier trimestre, ce qui a affecté la consommation. Mais cet effet négatif va disparaître progressivement, puisqu'il sera compensé par la baisse des cotisations salariales et la disparition progressive de la taxe d'habitation. Ce jeu de bonneteau fiscal n'explique pas les difficultés de l'économie française, lesquelles ne sont pas ponctuelles, pas plus qu'il n'apporte une quelconque solution à nos problèmes. La majorité évoque aussi des phénomènes conjoncturels comme la hausse des prix du pétrole et les incertitudes géopolitiques. D'accord, mais nos performances macroéconomiques restent en deçà de celles de bien d'autres pays soumis aux mêmes contraintes.
                
                
                    La deuxième explication est keynésienne : nos gouvernements seraient aveuglés par une idéologie pro-entreprise mise en application depuis la seconde partie du quinquennat de François Hollande. Depuis lors, priorité a été donnée à l'offre alors que la France souffrirait d'une demande insuffisante. Je ne perçois toujours pas le moindre début de logique dans cet argument. Le déficit annuel de notre commerce extérieur tourne autour de 60 milliards d'euros. Cela revient à dire que notre pays dépense largement plus qu'il ne produit. Dans ces conditions, augmenter plus rapidement nos dépenses publiques n'aboutira qu'à creuser davantage nos déficits, avec un impact marginal sur la croissance et l'emploi.
                
                
                    Pour un vrai budget de transformation
                    La troisième explication est à mon sens la seule correcte. Notre croissance peine à dépasser 1,5 % car ce taux correspond aux données structurelles de notre économie. 40 % des PME ont des difficultés à recruter, l'épargne des ménages finance les déficits publics et le logement social plutôt que l'innovation, et rien de crédible n'a été entrepris pour alléger les prélèvements obligatoires et défricher le maquis réglementaire dans lequel se débattent nos concitoyens. Il n'y a qu'une seule stratégie pertinente : continuer de libérer la production et le travail de tous les freins qui les entravent.
                
                
                    En pratique, cela signifie approfondir la loi Pacte de Bruno Le Maire pour provoquer un choc de simplification pour les entreprises, introduire une dégressivité de l'allocation chômage et entreprendre une réforme de l'Etat pour réduire la fiscalité. Sur ce dernier sujet, le projet de budget 2019, fondé sur le rabot et la baisse du pouvoir d'achat des retraités, paraît à côté de la plaque. Il reste quelques jours pour établir un vrai budget de transformation et non un bricolage "à la Hollande".
                
            