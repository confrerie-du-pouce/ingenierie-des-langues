
                Le patron de la Direction générale des finances publiques a tenu bon : la big bang de la retenue à la source, c'est son bébé.
                
                    Finalement, il a eu gain de cause. Inlassablement ces derniers jours, il a défendu sa réforme du prélèvement à la source, sourd aux critiques et aux attaques, bien décidé à mettre fin à 80 ans de tergiversations politiques sur le sujet. Lui, c'est Bruno Parent, le patron de la très puissante Direction générale des Finances publiques. Un état dans l'état né en 2008 de la fusion entre la DGI (Direction générale des impôts) et DGCP (Direction générale de la comptabilité publique). Une des plus grosses administrations fiscales de la planète, d'après un récent rapport de la Cour des comptes, avec près de 103 000 agents, en charge aussi bien de la collecte de l'impôt, du suivi de la dépense publique, de la gestion du cadastre que de la comptabilité des mairies.
                
                
                    L'homme n'est pas un de ces hauts fonctionnaires adeptes des aller-retour dans le secteur privé, histoire de faire fructifier réseau et portefeuille. Bruno Parent qui vient de fêter en août ses 64 ans est un pur commis de l'Etat, un technicien de la chose publique et des rouages fiscaux. Premier poste : 1er janvier 1979 à sa sortie de l'ENA - promotion "Droits de l'homme" - à la Direction générale des impôts. Il ne quittera plus le ministère de l'Économie, assistant tout au long de ces décennies à la valse des ministres.
                
                
                    La retenue à la source, la victoire d'un technicien
                    "Cette dernière victoire révèle la toute-puissance de la haute administration dont Parent est un des poids lourds, et de l'influence auprès de Macron du corps de l'Inspection générale des Finances", souligne un fin connaisseur de Bercy. L'homme a-t-il mis sa démission dans la balance si la réforme n'aboutissait pas ? "Ça n'est pas sa méthode", confie un de ses proches. Pourtant, en 2007 lorsque Nicolas Sarkozy lance la fusion entre la DGI et la DGCP, il demande à être démis de ses fonctions, étant opposé à ce rapprochement.
                
                
                    Ce qui le fait avancer ? Bousculer l'ordre établi, à l'image de ses cravates bariolées. Pas évident et presque schizophrénique lorsqu'on appartient à une administration aussi verrouillée que celle de Bercy. "C'est un infatigable modernisateur. Avec les années, il aurait pu s'user. Au contraire, il veut toujours remettre en cause les systèmes", confie à L'Express Vincent Mazauric, l'actuel directeur général de la Caisse nationale des allocations familiales avec lequel il a travaillé à la DGI.
                
                
                    Moderniser donc. En 2005, il est à l'origine d'un autre petite révolution fiscale : la déclaration pré-remplie. "Beaucoup doutaient déjà de cette réforme et dénonçaient les risques d'erreurs et de bugs. Après un an de test en Ile-et-Vilaine, il est allé présenter à Jean François Copé, alors ministre délégué au budget, les conclusions de l'expérimentation mais sans omettre les risques de la réforme, sans pipoter. Copé l'a suivi sans broncher", raconte un de ses anciens adjoints de l'époque.
                
                
                    Ce "démonteur de réveil", direct, cash, parfois cassant, "ne donne pas vraiment l'impression d'être sentimental" raconte un haut fonctionnaire du ministère de l'Economie. "Ça n'est pas vraiment le genre de directeur qui attend de savoir ce que pense le ministre avant de parler. Lui dit ce qu'il a en tête et l'assume. Quand on a lancé le chantier de la retenue à la source, il ne nous a caché aucune des difficultés techniques que nous allions rencontrer", confie à L'Express Michel Sapin, l'ancien ministre de l'Economie de François Hollande.
                
                
                    Son prochain chantier : fermer des trésoreries
                    A un an de la retraite, celui que certains voient comme le vrai patron de Bercy va devoir faire dans le social et l'humain. Son prochain gros chantier : une nouvelle vague de fermeture de trésoreries en province. Au début de l'été, un rapport de la Cour des comptes a mis le feu aux poudres, dénonçant une qualité de service inégale, et surtout la densité trop forte de son réseau territorial. Les chiffres sont éloquents. En 2015, la France comptait 2230 trésoreries, contre 570 en Allemagne, 247 en Italie ou 190 au Royaume-Uni. "Nombre de pays étrangers ont procédé au cours des dernières années à la rationalisation de leur réseau fiscal, tirant parti du développement de la dématérialisation", soulignent les Sages de la rue Cambon.
                
                
                    Sur ce dossier, les syndicats l'attendent de pied ferme. "On est d'accord sur rien. On ne va quand même pas participer à la désertification des zones rurales et à la fuite des services publics", tonne Helène Fauvel, la secrétaire générale de FO DGfip. Une bataille peut être moins médiatique que celle du prélèvement à la source, mais sans doute aussi difficile à mener.
                
            