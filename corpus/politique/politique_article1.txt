
                Un entretien accordé par l'animateur télé a contraint l'Elysée et deux ministres à monter au front pour la mission patrimoine
                
                    Stéphane Bern quittera-t-il en fin d'année la mission sur le patrimoine que lui a confiée Emmanuel Macron? Ce samedi, après une semaine assez agitée avec le départ fracassant de Nicolas Hulot du gouvernement, un entretien accordé par l'animateur vient bousculer le week-end de l'exécutif, contraint à réagir assez rapidement.
                
                
                    Petit retour sur une histoire qui se décline en quatre temps.
                
                
                    "Cache-misère", "pantin": un entretien et des mots qui claquent
                    Au coeur d'une interview parue ce vendredi dans les journaux du groupe Ebra, Stéphane Bern évoque "du bon et du moins bon" dans la mission sur le patrimoine qu'on lui a confié et qui est destinée à aider le patrimoine local en péril. Il se dit "satisfait d'avoir réveillé l'intérêt des Français pour cette cause" et de l'organisation du loto du patrimoine du 14 septembre et du lancement d'un jeu à gratter, qui devraient permettre de récolter 15 à 20 millions d'euros.
                
                
                    Mais il insiste surtout sur ses frustrations. "J'entends qu'on est prêt à mobiliser 450 millions d'euros pour rénover le Grand Palais à Paris. Et pendant ce temps, on me laisse me décarcasser pour trouver 20 millions d'euros pour le patrimoine vernaculaire des petits villages", regrette l'animateur.
                
                
                    Stéphane Bern s'attaque par ailleurs à la loi sur le logement portée par Jacques Mézard, ministre de la Cohésion des Territoires. Selon lui, elle va permettre de "détruire des quartiers entiers" en affaiblissant le rôle des architectes des bâtiments de France (ABF), corps professoral chargé de surveiller les travaux sur le patrimoine.
                
                
                    Alors "si tout cela n'est qu'un effet d'annonce, je partirai. Je ne veux pas être un cache-misère", insiste-t-il, évoquant la "fin de l'année" pour faire son bilan. "Si je vois que je ne sers à rien, que je n'étais qu'un pantin et qu'on s'est servi de moi, je reviendrai à mon travail" en continuant le combat pour le patrimoine, a-t-il ajouté, notant qu'il militait déjà dans ce domaine "quand le président Macron n'était pas encore né".
                
                
                    L'Elysée monte au créneau; "pas de 'hulotte'", selon Bern
                    Il n'en a pas fallu beaucoup plus pour que l'entourage du président de la République ne monte au front. Et ce, dès samedi matin. "Ses propos ont été mal compris", soutient l'Elysée à BFMTV. "Il voulait alerter et sensibiliser sur le patrimoine. Il fait sur ce sujet un formidable travail et à été soutenu par un véritable engagement du président de la République".
                
                
                    Des propos qui n'ont pas manqué de faire réagir le journaliste qui a recueilli les propos de l'animateur, qui dément toute incompréhension... avant que Stéphane Bern ne reprenne la parole pour se lancer dans un subtil exercice. A savoir, confirmer ses propos tout en évitant d'attiser des tensions trop fortes avec le Chateau.
                
                
                    "Je ne fais pas une 'hulotte', je n'ai pas l'intention de partir avec pertes et fracas, je tire un signal d'alarme", déclare-t-il dans la foulée sur franceinfo.
                
                
                    "J'ai le sentiment, parfois en tout cas, qu'il y a une démobilisation de l'État, et qu'au fond, on me dit: 'C'est à vous de trouver les moyens, les nouvelles sources de financement pour sauver le patrimoine en péril en France'", précise-t-il.
                
                
                    Avant d'ajouter: "J'ai peu vu, je vous l'avoue, le chef de l'État. Je l'ai vu lorsqu'il a réuni les porteurs de projets au mois de mai dernier. Je n'ai pas le sentiment pour le moment d'être une marionnette, un pantin, ou d'être utilisé. Si j'avais ce sentiment, j'ai dit - et cela fait le buzz médiatique, vous en conviendrez - que je partirais immédiatement."
                
                
                    Nyssen réagit aussitôt...
                    Deux ministres se sont également exprimés sur le sujet, ceux qui sont en première ligne dans l'entretien accordé par Bern. A savoir, François Nyssen et Jacques Mézard.
                
                
                    La ministre de la Culture a ainsi rappelé que la rue de Valois "pilote 6 000 opérations de restauration chaque année, pour préserver notre patrimoine riche de 44 000 monuments historiques, ce n'est pas exactement ce qu'on peut appeler de la misère". Une augmentation de 5% "sanctuarisée pour la durée du quinquennat", soit "326 millions d'euros consacrés chaque année à la préservation" d'un patrimoine à la diversité "inégalable", a-t-elle ajouté.
                
                
                    .. suivie de près par Jacques Mézard
                    De son côté, Jacques Mézard, ministre de la Cohésion des Territoires, a indiqué sur BFMTV qu'il "comprend parfaitement que Stéphane Bern considère que lui, pour sa mission, n'a pas suffisamment de moyens (...), mais il n'y a pas que ces quinze à vingt millions qui sont dirigés sur le patrimoine" par l'Etat.
                
                
                    "Il ne s'agit pas de supprimer l'avis des architectes des bâtiments de France, il s'agit dans deux cas, antennes de téléphonie mobile et puis habitat indigne, de venir à un avis simple" et donc non contraignant, a détaillé le ministre. "S'il y a encore un grand patrimoine de qualité dans nos villes et nos villages c'est grâce à l'action des collectivités locales", a-t-il insisté.
                
            