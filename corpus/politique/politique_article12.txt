
                Après la démission surprise de la ministre des Sports Laura Flessel, la vice-championne olympique fait son entrée au gouvernement.
                
                    L'ancienne nageuse Roxana Maracineanu a été nommée ministre des Sports ce mardi, après la démission soudaine de Laura Flessel, a annoncé l'Elysée dans un communiqué.
                
                
                    Née en 1975 à Bucarest, en Roumanie, Roxana Maracineanu est arrivée en France à l'âge de 8 ans, en 1984, après que ses parents ont fui la dictature de Nicolae Ceausescu. Installée à Mulhouse avec sa famille, qui a obtenu le statut de réfugiés, elle intègre l'équipe de France de natation en 1993, et devient la première Française à remporter le championnat du monde de natation sur 200 mètres dos en 1998. Elle décroche l'argent aux Jeux de Sydney en 2000.
                
                
                    La sportive met fin à sa carrière en 2004, après quoi elle intègre une grande école de commerce parisienne, où elle obtient un bac+5. Elle devient ensuite consultante pour France Télévisions, L'Equipe TV ou encore Europe 1 lors de différentes compétitions sportives, comme les Jeux olympiques de Pékin en 2008. En 2015, elle avait été candidate à au poste de directeur technique national de la Fédération française de natation, finalement attribué à Jacques Favre.
                
                
                    Formation à la nage
                    Son premier engagement en politique remonte à 2010, année où elle a été élue conseillère régionale d'Île-de-France sur une liste apparentée Parti socialiste. Après la fin de sa carrière de nageuse, elle avait également créé deux associations destinées, notamment, à l'accompagnement des enfants, Educateam et "J'peux pas, j'ai piscine".
                
                
                    Dans la lignée de ces engagements, l'ancienne sportive était chargée depuis deux mois du programme du gouvernement pour la formation à la nage dans les écoles, alors que le nombre de noyades est en hausse depuis 2015.
                
                
                    Le gouvernement souhaite "mobiliser l'ensemble des acteurs concernés pour mieux comprendre les origines de cette tendance et mettre au point un nouveau plan de lutte contre les noyades".
                
                
                    M. Philippe avait demandé à Mme Maracineanu d'accompagner cette mission, elle qui avait plaidé début juillet dans Le Parisien pour des initiations à la nage plus poussées, regrettant les méthodes actuelles.
                
                
                    Ambassadrice des JO de 2024
                    L'arrivée de Roxana Maracineanu est largement saluée par Denis Auguin, ancien entraîneur d'Alain Bernard, aujourd'hui chargé de la coordination des équipes de France de natation. Contacté par l'Express, il assure qu'il s'agit d'une "bonne ambassadrice de Paris 2024". "Elle connaît très bien les Jeux olympiques, elle en a une connaissance de terrain. Après, son objectif sera déjà d'inverser la baisse des dotations du ministère des sports (Selon L'Equipe, le budget des Sports de 2019 serait en baisse de 6,2 % par rapport à 2018). Ce serait beau".
                
                
                    Roxana Maracineanu est "quelqu'un qui a un profil politique", ajoute Denis Auguin. "Elle a des convictions très fortes sur l'apprentissage de la natation. Elle laisse transparaître une vision grand public plus qu'un projet axé sur le sport de haut niveau. C'est important dans un ministère des Sports [...] Humainement, elle a montré en tant que sportive des qualités de courage et d'engagement. Opiniâtre, elle ne se laisse pas faire".
                
            