
                Deux anciens vice-présidents du groupe ont démissionné et organisent une réunion mercredi pour encourager d'autres départs.
                
                    Avis de tempête dans le fief de Marine Le Pen. Le groupe Rassemblement national (RN, ex-FN) du conseil régional des Hauts-de-France pourrait voir un nombre conséquent d'élus mécontents claquer la porte. Selon les informations de L'Express, deux d'entre eux viennent de le faire : André Murawski, ancien vice-président du groupe chargé des ressources, a quitté le groupe le 11 juillet dernier et le RN le lendemain. Alexis Salmon, ancien vice-président responsable de la communication, a démissionné du groupe, le 1er septembre, et ne renouvellera pas son adhésion au parti l'année prochaine.
                
                
                    Les deux conseillers ne sont pas les premiers à partir. De février à avril 2017, trois conseillers régionaux - Olivier Delbe, Florence Italiani et Chantal Lemaire - étaient déjà partis. Six autres avaient démissionné dans la foulée du départ de Florian Philippot. De 54 élus à l'origine, le groupe RN est donc passé à 43 aujourd'hui. Mais André Murawski et Alexis Salmon ne comptent pas en rester là.
                
                
                    Une réunion conspirative
                    Mercredi soir, ils organisent à Arras une réunion d'information à laquelle ont été invités des conseillers régionaux qui s'interrogent sur l'opportunité de créer un nouveau groupe (il faut au moins dix personnes) au sein de l'hémicycle régional. Selon des sources concordantes, au moins une quinzaine d'élus seront présents, dont une dizaine actuellement membres du groupe RN. Un précédent existe en région Grand Est. En mars, sept conseillers RN ont fait sécession pour constituer un nouveau groupe composé de membres du Centre national des indépendants et paysans (Cnip) et de divers droite.
                
                
                    Les deux conseillers régionaux rebelles ne cachent pas que l'objectif est bien de créer un groupe dissident au RN. Un camouflet potentiel pour Marine Le Pen, élue conseillère régionale des Hauts-de-France. Le nouveau groupe pourrait siéger sous le label de Debout la France (DLF), le parti de Nicolas Dupont-Aignan, ou encore s'unir avec quelques élus LR. André Murawski comme Alexis Salmon ont rencontré Nicolas Dupont-Aignan au cours de l'été. La responsable Ile-de-France de DLF, Yasmine Benzelmat, elle-même démissionnaire du groupe RN en Ile-de-France, assistera à la réunion à Arras. "Je viens partager mon expérience", glisse-t-elle.
                
                
                    Des tensions locales...
                    Pour expliquer leur départ, André Murawski et Alexis Salmon évoquent aussi bien des tensions internes au groupe régional que des problèmes nationaux. Au niveau local, André Murawski s'est fait retirer en mai une partie de sa délégation aux ressources par Marine Le Pen. Il évoque un "problème de gestion du personnel" par le président de groupe Philippe Eymery. Alexis Salmon complète : "Par sa personnalité, le député et conseiller régional Sébastien Chenu, par ailleurs délégué départemental du Nord, cristallise les tensions. "Il dit ressentir un "sentiment de favoritisme de la part de Marine Le Pen et d'injustice".
                
                
                    Mais au-delà des bisbilles locales, l'ombre des affaires judiciaires, notamment celle des emplois présumés fictifs d'assistants parlementaires au Parlement européen, inquiète ces élus. "C'est déjà dur de porter l'étiquette RN, mais s'il y a des questions éthiques qui se posent, on dit stop", fulmine Alexis Salmon. "L'argument selon lequel les accusations portées contre le RN sont politiques, ce n'est plus suffisant. Il y a un souci. Quant aux 9 millions d'euros, empruntés à une banque russe et que l'on ne peut pas rembourser, ça pose problème", estime-t-il.
                
                
                    ...et nationales
                    Dans un article paru sur le site Polemia en juillet, André Murawski analysait : "L'examen des comptes du Front national entre 2011 et 2016, qui coïncide avec la présidence de Marine Le Pen, montre que si les ressources de ce parti ont considérablement augmenté pendant cette période, les charges ont augmenté plus rapidement ce qui a conduit à des résultats constamment déficitaires entre 2012 et 2016, et à un endettement sans cesse accru." Il ajoute : "Dans ce contexte, peut-on soutenir que le Front national de Marine Le Pen est bien géré ? Il est permis d'en douter comme il est permis de craindre ce qui surviendrait si ce parti accédait un jour aux responsabilités du pouvoir en France", ajoute l'élu.
                
                
                    "Personne ne sait sur quoi va déboucher la réunion de mercredi, confie anonymement un conseiller régional RN qui y participera. Cela fait de nombreuses années que je milite au RN. Mais le parti traverse une crise sans commune mesure. S'il y a un nouveau groupe qui se crée, j'en serais ravi. De toute façon, je ne sens plus ma place au sein des conseillers RN..."
                
                
                    Contacté par L'Express, Philippe Eymery, le président du groupe RN au conseil régional des Hauts-de-France, n'était pas joignable lundi après-midi.
                
            