
                Porté disparu une journée, Abhilash Tomy a depuis fait savoir qu'il est "en sécurité"... mais sa situation inquiète.
                
                    Blessé, à la dérive et toujours en danger. Porté disparu depuis vendredi matin, le skipper indien Abhilash Tomy a donné signe de vie: il s'est dit "en sécurité", selon les organisateurs du Golden Globe Race, course à la voile laquelle participait le navigateur. Mais tout reste à faire pour lui venir en aide, alors qu'il se situe à plus de 3700 km de l'Australie.
                
                
                    Depuis la mi-journée vendredi, les organisateurs de la course à voile ne disposaient que d'un laconique et "inquiétant" message de Tomy: "Retourné. Démâté. Sévère blessure au dos. Ne peux pas me lever".
                
                
                    "Aux premières lueurs de la matinée", ils en ont reçu un nouveau: "Extrêmement difficile de marcher, peut-être besoin de civière, ne peux pas marcher, heureusement en sécurité à l'intérieur du bateau. Impossible d'atteindre le 2e YB3 (l'unité de messagerie portable Yellow Brick, NDLR) ou quoi que ce soit. Téléphone satellite hors service".
                
                
                    "Aussi éloigné que l'on puisse l'être de toute aide"
                    Le navigateur indien est "immobilisé sur sa couchette à l'intérieur du bateau après avoir été roulé par une vague" et se trouve actuellement en plein Océan Indien à 2 000 milles nautiques (environ 3 700 km) de l'Australie.
                
                
                    Autrement dit, résume le Golden Globe Race dans un communiqué, il "est aussi éloigné que l'on puisse l'être de toute aide".
                
                
                    Abhilash Tomy est l'un des 17 marins qui ont pris le départ au 1er juillet de cette course, partie des Sables-d'Olonne, qui doit durer entre huit à dix mois à bord de petits voiliers dépourvus de moyens modernes, dans l'esprit des pionniers de la course autour du monde en solitaire.
                
                
                    Le temps "va jouer" contre le navigateur
                    Depuis vendredi matin, tous les moyens sont en tout cas mis en oeuvre pour porter secours à Abhilash Tomy. "Le Centre australien de coordination des opérations de sauvetage travaille d'arrache-pied pour évaluer et coordonner toutes les options possibles de sauvetage", souligne encore les organisateurs de la course quelques heures après l'activation du "code rouge".
                
                
                    Un appel à tous les navires a notamment été envoyé par la Défense australienne pour tenter de lui porter secours. Le Centre de secours maritime français, basé à La Réunion, a également été sollicité, tout comme la Marine indienne, a-t-on appris de même source, le "commandant Tomy" étant "un officier de l'aviation navale".
                
                
                    Interrogé par Franceinfo, Alain Gautier, navigateur et consultant sécurité sur le Vendée Globe Challenge, considère qu'Abhilash Tomy est dans une "situation vraiment problématique" et dangereuse. "Un bateau sans mât est extrêmement vulnérable puisqu'il est à la merci des vagues (...) sans vitesse et avec un marin qui n'est pas capable de mener son bateau, le bateau peut aussi se retourner une autre fois." Avant d'ajouter, non sans inquiétude: "le temps va jouer contre ce skipper indien".
                
            