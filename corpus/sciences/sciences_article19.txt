
                En déplacement ce jeudi dans les Antilles, Emmanuel Macron a demandé "une réponse en profondeur" au fléau des algues brunes.
                
                    En visite en Martinique et en Guadeloupe depuis jeudi, le président a désigné l'apparition massive de ces algues brunes toxiques comme l'une des "conséquences directes du réchauffement climatique".
                
                
                    Le phénomène est particulièrement important cette année. Ce jeudi, un collectif guadeloupéen a ainsi déposé une plainte à Paris pour mise en danger d'autrui. Il dénonce les "insuffisances criantes" de l'État "face à l'envahissement des côtes antillaises" par les sargasses.
                
                
                    En juin dernier, plusieurs ministres s'étaient déjà rendus sur les lieux pour constater les dégâts de cette pollution végétale aux lourdes conséquences économiques. Et d'annoncer un plan de lutte de 10 millions d'euros sur deux ans, dont l'objectif était de réduire le délai de ramassage à 48h après les échouages.
                
                
                    Une apparition massive
                    Les sargasses envahissent les îles de la Guadeloupe et de la Martinique. Ces territoires ultramarins français subissent, comme le reste des Antilles, l'apparition massive d'algues brunes toxiques. Une pollution végétale marine aux lourdes conséquences pour l'économie touristique, les entreprises de pêche ou les compagnies maritimes.
                
                
                    Ce problème a débuté en 2011, mais n'a "jamais été aussi important que cette année", expliquait alors Annick Girardin. La ministre des Outre-mer et Nicolas Hulot, son confrère d'alors au ministère de la Transition écologique, s'étaient spécialement rendus sur place pour annoncer et détailler les mesures prises par l'Etat.
                
                
                    Face à cette crise des sargasses, les ministres avaient constaté l'ampleur surprenante des dégâts sur des kilomètres de rivage. Début mai, les algues brunes ont pris d'assaut en nombre l'entrée des ports, bloquant les hélices des bateaux et coupant partiellement du monde ces îles ainsi que celles de La Désirade, de Marie-Galante ou encore des Saintes. Les deux envoyés de la métropole avaient annoncé un plan de 10 millions d'euros pour lutter contre ce phénomène, qui concerne également la Guyane et les îles du Nord.
                
                
                    Les habitants s'inquiètent particulièrement de la toxicité de l'algue, dont la dégradation émet notamment de l'ammoniac et du sulfure d'hydrogène, des gaz à forte odeur provoquant maux de tête, nausées et vomissements. Les animaux marins en souffrent aussi tout comme même les moteurs des bateaux s'abîment à son contact.
                
                
                    L'Agence régionale de santé (ARS) conseille l'éloignement aux femmes enceintes, bébés, personnes âgées, asthmatiques, etc. Au-delà du seuil d'une concentration de 5 ppm, seuls "les professionnels aux moyens de mesures avec alarmes" sont autorisés à s'approcher des amas de sargasses. Par précaution, la commune de Petit-Bourg avait alors fermé ses écoles, soit 2 000 élèves et huit établissements concernés. Certaines plages demeurent interdites à la baignade.
                
                
                    Ces algues nauséabondes, dérivant sous forme de radeaux, proviennent de nouveaux lieux de prolifération qui n'ont pas encore été localisés avec certitude. "L'analyse d'images satellites a montré l'existence d'une nouvelle zone d'accumulation des sargasses située au nord du Brésil dans l'océan Atlantique central", soulignait le gouvernement français dès 2015. Le problème s'étend d'ailleurs à l'ensemble de la région : Cancun au Mexique, de l'autre côté des Caraïbes, subit le même problème sur ses plages de sable blanc où s'alignent les hôtels à touristes.
                
                
                    Le plan français annoncé en juin prévoit 22 nouveaux capteurs pour surveiller l'échouage et les émanations, afin d'établir un bulletin quotidien. Son financement doit permettre des ramassages "48h après les échouages", avant la décomposition. Les techniques se multiplient justement au banc d'essai : une mission du sénateur guadeloupéen Dominique Théophile devait rendre dans les îles voisines de la Caraïbe à la recherche de solutions innovantes de ramassage et de valorisation.
                
                
                    Une étude de la Chambre de Commerce et d'industrie des îles de Guadeloupe estime que cette crise des sargasses représentait au seul premier trimestre 2018 environ 4,5 millions d'euros de pertes pour les entreprises locales. L'Agence de l'environnement et de la maîtrise de l'énergie (Ademe), de son côté, avait évalué il y a trois ans le "gisement d'algues" à 60 000 tonnes par an rien que pour la Guadeloupe... Preuve que l'absence de solutions était pesante dès cette époque.
                
            