
                L'administrateur de la Nasa songe à collaborer avec des annonceurs, mais les astronautes ne sont pas convaincus.
                
                    Si elle connaît l'espace comme sa poche, la Nasa a encore un tabou: les espaces publicitaires. L'administrateur de l'agence spatiale américaine, Jim Bridenstine, a récemment annoncé lors d'une réunion d'experts son intention d'ouvrir les fusées et autres vaisseaux de la Nasa aux annonceurs privés, révèle le Washington Post. L'objectif, faire des économies en stimulant l'intérêt du privé pour la présence américaine dans l'espace.
                
                
                    De l'aveu de l'administrateur, une telle suggestion est "provocatrice", mais "suscite de l'intérêt en ce moment", a-t-il déclaré lors de cette réunion, rapporte le quotidien américain. Un comité a été établi pour étudier sa faisabilité. Une autre idée consiste à permettre aux astronautes d'apparaître dans des publicités et à plaquer leur visage sur des emballages de céréales. "J'aimerais voir des enfants grandir en n'ayant pas forcément envie de devenir un sportif professionnel, mais aussi un astronaute ou un scientifique de la Nasa", a expliqué l'administrateur.
                
                
                    Tourner des films dans l'ISS
                    Donald Trump devrait apprécier l'idée. En février dernier, le même Washington Post évoquait l'intention de l'administration américaine de privatiser la Station spatiale internationale (ISS). Un projet fraîchement accueilli, mais qui, sur le papier, pourrait rapporter gros. Selon une étude réalisée en 2017 par le Science and Technology Policy Institute et citée par le quotidien, l'ISS privatisée pourrait rapporter entre 455 millions et 1,2 milliard de dollars par an.
                
                
                    L'étude citait notamment le "naming", idée suggérée par Jim Bridenstine pour les appareils de la Nasa, une opération consistant à associer le nom d'une marque à un lieu ou à une compétition sportive - comme la "Ligue 1 Conforama" en France. Des entreprises ont déjà payé jusqu'à 20 millions de dollars pour qu'une enceinte sportive porte leur nom, notait l'étude. Le tournage de films, l'accueil de touristes et la location de modules de la station étaient aussi mentionnés.
                
                
                    Pudeur sur les M&M's
                    Et les astronautes, dans tout ça ? Interrogé par le Washington Post, Scott Kelly est sceptique. L'homme qui a passé un an à bord de l'ISS voit un "renversement majeur" dans l'idée d'apposer des logos sur les fusées et d'associer des astronautes à des marques, en raison d'une règle qui interdit aux fonctionnaires de s'enrichir dans le cadre de leur mission de service public. Un autre astronaute craint que le Congrès soit tenté de réduire le budget de l'agence spatiale.
                
                
                    Si la Nasa collabore déjà avec des entreprises privées dans le cadre de sa mission, elle prend garde à ne pas donner l'impression de favoriser des marques éloignées de son secteur. Ainsi, en 2011, lors du lancement d'une navette spatiale, les bonbons M&M's thématiques fournis aux astronautes devenaient des "chocolats recouverts de sucre", rapportait Space.com. Tous les pays n'ont pas cette prudence. Ainsi, en 1999, un logo Pizza Hut de 9 mètres de haut avait été collé sur une fusée russe, annonçait le New York Times.
                
            