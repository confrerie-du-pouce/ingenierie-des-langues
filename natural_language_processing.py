import os
import spacy

from nltk import ngrams


class StatisticalModel(object):

    def __init__(self) -> None:
        super().__init__()
        self.nlp = spacy.load('fr_core_news_md')
        
        self.word = set()
        self.frequency_word = dict()
        self.word_ngrams = dict()

        self.lemmas = set()
        self.frequency_lemma = dict()
        self.lemma_ngrams = dict()
        
        self.__word_counter = 0

    def compute_directory(self, path: str):
        for sub_path in os.listdir(path):
            if os.path.isdir(sub_path):
                self.compute_directory(sub_path)
            if os.path.isfile(sub_path):
                self.compute_file(sub_path)

    def compute_file(self, path: str):
        text = open(path, 'r', encoding='utf-8').read()
        doc = self.nlp(text)
        for token in doc:
            self.word.add(token.text)
            if token.text not in self.frequency_word.keys():
                self.frequency_word[token.text] = 0
            self.frequency_word[token.text] += 1

            self.lemmas.add(token.lemma_)
            if token.text not in self.frequency_lemma.keys():
                self.frequency_lemma[token.text] = 0
            self.frequency_lemma[token.text] += 1

            self.__word_counter += 1

        for i in range(1, 6):
            self.extract_ngrams(i, text)

    def extract_ngrams(self, n: int, text: str):
        if not self.ngrams[n]:
            self.ngrams[n] = []
        self.ngrams[n].append(ngrams(text, n))
