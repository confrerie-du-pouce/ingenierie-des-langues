# Ingénierie des langues

## Installation

L'application nécessite des library python supplémentaire :

- Flask (server web) : `pip3 install flask`
- SpaCy (NLP) : `pip3 install spacy`
  - `fr_core_news_md` : `python3 -m spacy download fr_core_news_md`

Pour démarrer le serveur, il faut lancer ces deux commandes :
```bash
export FLASK_APP=server.py
flask run
```

Une fois la le serveur initialisé, ouvrez votre navigateur sur [localhost:5000](http://localhost:5000)
