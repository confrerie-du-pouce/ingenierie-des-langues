import re
import spacy
from random import randrange
from random import shuffle
import json
import os


# nlp = spacy.load('fr_core_news_md')


def tokenizeSentence(text):
    """
    fonction that tokenize a text, each token represent a sentence
    :param text: string representing the corpus
    :return: list of string representing tokens
    """
    tokens = []
    text = re.sub(r'[^a-zA-ZáàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ.\s]', ' ', text)
    text = re.sub(r'[\t\r\n\f\v]', '', text)
    text2 = " ".join(re.split(r"\s+", text))
    for sentence in text2.split("."):
        if sentence != '':
            tokens += [sentence.strip()]
    return tokens


def tokenizeWord(text):
    """
    fonction that tokenize a text, each token represent a word
    :param text: string representing the corpus
    :return: list of string representing tokens
    """
    tokens = []
    text = re.sub(r'[^a-zA-ZáàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ.,?!\s]', ' ', text)
    text = re.sub(r'([?!,.]+)', r' \1 ', text)
    for word in text.split():
        tokens += [word]
    return tokens


def tokenizeSpacy(text):
    """
    fonction that tokenize a text with spacy module
    :param text: string representing the corpus
    :return: a list of different kind of token
    """
    text = text.lower()
    text = re.sub(r'[^a-zA-ZáàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ\s]', ' ', text)
    text = re.sub(r'[\t\r\n\f\v]', '', text)
    text2 = " ".join(re.split(r"\s+", text))
    return nlp(text2)


def generateNGrams(tokens, lenght=2):
    """
    fonction that generate ngrams of the given length
    :param tokens: list of tokens from a corpus
    :param lenght: int representing the lenght for the ngrams
    :return: list of ngrams of given length
    """
    ngrams = []
    ngram = ""
    for i in range(len(tokens)):
        if i + lenght <= len(tokens):
            for k in range(lenght):
                ngram += tokens[i + k] + " "
            ngrams += [ngram.strip()]
            ngram = ""
    return ngrams


def getNgramNumber(ngrams):
    """
    fonction that return the number of ngram
    :param ngrams: list of ngrams
    :return: int representing the number of ngram of the list given
    """
    return len(ngrams)


def getUniqueNgramNumber(ngrams):
    """
    fonction that gives the number of unique ngram
    :param ngrams: list of ngrams
    :return: int representing the number of unique ngram of the list given
    """
    unique = 0
    for i in range(len(ngrams)):
        nb = ngrams.count(ngrams[i])
        if nb == 1:
            unique += 1
    return unique


def getNgramFrequency(ngrams, ngram):
    """
    fonction that get a ngram frequency
    :param ngrams: list of ngrams
    :param ngram: string representing a ngram
    :return: int representing the ngram frequency
    """
    return ngrams.count(ngram)


def getAllNgramFrequency(ngrams):
    """
    fonction that get all ngrams frequency
    :param ngrams: list of ngrams
    :return: list of ngrams with their frequency
    """
    frequencies = []
    for i in range(len(ngrams)):
        already = False
        for j in range(len(frequencies)):
            if frequencies[j][0] == ngrams[i]:
                already = True
        if already == False:
            frequencies += [[ngrams[i], getNgramFrequency(ngrams, ngrams[i])]]
    return frequencies


def sortNgramByFrequenciesWithFrenquency(frequencies):
    """
    fonction that sort ngrams by their frequency
    :param frequencies: list of ngrams with their frequency
    :return: list of ngrams sorted by their frequency
    """
    return sorted(frequencies, key=lambda x: x[1], reverse=True)


def getLemmas(tokens):
    """
    fonction to get the lemma token from a list of spacy tokens
    :param tokens: list of spacy token
    :return: list of string representing lemma
    """
    return [token.lemma_ for token in tokens]


def getPOStagtoken(tokens):
    """
    fonction to get the POS tag token from a list of spacy tokens
    :param tokens: list of spacy tokens
    :return: list of string representing POS tag
    """
    return [token.pos_ for token in tokens]


def convertNgramSentenceToNgramList(ngrams):
    """
    fonction that convert ngram sentence to a ngram list
    :param ngrams: list of ngrams sentences to be converted
    :return: list of ngram converted
    """
    res = []
    for i in range(len(ngrams)):
        res += [ngrams[i].split()]
    return res


def ngramAllWordProbability(ngrams, words):
    """
    fonction that calculate the probability of all word that can be in the ngram given
    :param ngrams: list of all ngrams
    :param words: list of an incomplete ngram to be completed
    :return: probability of all words for the given ngram that can be associated to him
    """
    length = len(words)
    if length > len(ngrams[0]) and length < len(ngrams[0]) - 1:
        return
    done = []
    res = []
    j = 0
    for i in range(len(ngrams)):
        if ngrams[i][:length] == words:
            if ngrams[i][length] not in done:
                done += [ngrams[i][length]]
                res += [[ngrams[i][length], ngramWordProbability(ngrams, words, ngrams[i][length])]]
    return res


def ngramWordProbability(ngrams, words, word):
    """
    fonction that calculate the probability of a word in a ngram (Markov assumption)
    :param ngrams: list of all ngrams
    :param words: list that represent only one ngram without his last word
    :param word: string of a word that fit the ngram
    :return: the probability that the word can be in the ngram
    """
    prob = wordsFrequency(ngrams, words, word)
    return prob[1] / prob[0]


def wordsFrequency(ngrams, words, word):
    """
    fonction that calculate frequency of a ngram without his last word ( if it's a trigram, it takes only the two first
    words) and also calculate frequency of a word given which is the last word of the given ngram.
    :param ngrams: list of all ngrams
    :param words: list that represent only one ngram without his last word
    :param word: string of a word that fit the ngram
    :return: list of two frequencies, the first one is the frequency of the ngram and the second one the last word of it
    """
    res = 0
    res2 = 0
    size = len(ngrams[0]) - 1
    for i in range(len(ngrams)):
        ngram = []
        for j in range(size):
            ngram += [ngrams[i][j]]
        if ngram == words:
            res += 1
            if ngrams[i][size] == word:
                res2 += 1
    return [res, res2]


def pickRandomWord(text):
    """
    fonction that pick a random word from the text given
    :param text: string representing the text
    :return: string, a random word from the text
    """
    text = text.lower()
    text = re.sub(r'[^a-zA-ZáàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ.\s]', ' ', text)
    text = re.sub(r'[\t\r\n\f\v]', '', text)
    splitted = text.split()
    random = randrange(len(splitted))
    return splitted[random]


#
def sortProb(list):
    """
    fonction that sort a list of word probability from the highest probability to the lowest
    :param list: list of words with their probability
    :return: the list sorted
    """
    return sorted(list, key=lambda x: x[1], reverse=True)


def generateSentenceBeginingWithoutLast(ngrams, length):
    """
    Fonction that generate a begining of a sentence by taking the first word with a capital letter in the ngrams shuffled
    and make the ngram related to the first ngram of the sentence
    :param ngrams: list of all ngrams
    :param length: length of the ngrams
    :return: list that represent the ngram choosen for the sentence begining
    """
    ngramscopy = copyAndShuffleList(ngrams)
    stop = False
    index = 0
    res = []
    while stop != True:
        if ngramscopy[index][0].isupper():
            res = ngramscopy[index].split()
            del res[-1]
            stop = True
        index += 1
    return res


def generateSentenceBegining(ngrams, length):
    """
    Fonction that generate a begining of a sentence by taking the first word with a capital letter in the ngrams shuffled
    and make the ngram related to the first ngram of the sentence
    :param ngrams: list of all ngrams
    :param length: length of the ngrams
    :return: list that represent the ngram choosen for the sentence begining
    """
    ngramscopy = copyAndShuffleList(ngrams)
    stop = False
    index = 0
    res = []
    while stop != True:
        if ngramscopy[index][0].isupper():
            res = ngramscopy[index].split()
            stop = True
        index += 1
    return res


def copyAndShuffleList(list):
    """
    fontcion that copy a list and shuffle it
    :param list: list to be copied and shuffled
    :return: copied list shuffled
    """
    new = []
    for i in range(len(list)):
        new += [list[i]]
    shuffle(new)
    return new


def generateSentence(ngrams, word, sentenceSize):
    """
    unused function that generate sentence
    :param ngrams: list of all ngrams
    :param word: list of the firsts words of the sentence
    :param sentenceSize: int that represent the size of the sentence
    :return: string of the generated sentence
    """
    word = word
    last = word
    for i in range(sentenceSize):
        x = sortProb(ngramAllWordProbability(convertNgramSentenceToNgramList(ngrams), last))
        if len(x) > 0:
            if x[0][0] in word and len(x) > 1:
                if ngramsize > 2:
                    last = last[-(ngramsize - 2):] + [x[1][0]]
                    word += [x[1][0]]
                else:
                    last = [x[1][0]]
                    word += [x[1][0]]
            else:
                if ngramsize > 2:
                    last = last[-(ngramsize - 2):] + [x[0][0]]
                    word += [x[0][0]]
                else:
                    last = [x[0][0]]
                    word += [x[0][0]]
    wordstr = " ".join(word)
    return wordstr.strip()


def saveInJSONFile(name, tosave, length):
    """
    fonction used to store data in json
    :param name: string name of the json file
    :param tosave: data to be saved
    :param length: key for the dictionnary, representing ngram size
    """
    if not os.path.isfile('alldatas/' + name + '.json'):
        createJSONFile(name)
    addToJSONFile(name, tosave, length)


def createJSONFile(name):
    """
    function called by saveInJSON if the file to store data don't exist
    :param name: string name of the file to be created
    """
    f = open("alldatas/" + name + ".json", 'w')
    dict = {}
    json.dump(dict, f)


def addToJSONFile(name, tosave, length):
    """
    function that add data to a json file
    :param name: string name of the file
    :param tosave: data to store
    :param length: key for the dictionnary, representing ngram size
    """
    read = open("alldatas/" + name + ".json")
    loaded = json.load(read)
    write = open("alldatas/" + name + ".json", "w")
    if str(length) in loaded.keys():
        loaded[str(length)] += tosave
    else:
        loaded[str(length)] = tosave
    json.dump(loaded, write)


def getFromJSONFile(name, length):
    """
    function that get data from a JSON file
    :param name: name of the file
    :param length: key for the dictionnary, representing ngram size
    :return: list of the data asked
    """
    f = open("alldatas/" + name + ".json")
    loaded = json.load(f)
    return loaded[str(length)]


def getAllNgramOfNewCategoryAndProbabilities(name, text, length):
    """
    function that save in a JSON file all ngram for a category
    :param name: string name of the category, also used for the file name
    :param length: int representing ngram size
    """
    tokens = tokenizeWord(text)
    ngrams = generateNGrams(tokens, length)
    saveInJSONFile(name + "ngrams", ngrams, length)
    probabilities = getAllNgramWithLastWordsProbability(ngrams, length)
    saveInJSONFile(name, probabilities, length)


def getAllNgramOfCategoryProbability(name, length):
    """
    function that save in a JSON file all the next word probability of a ngram for a category
    :param name: string name of the category, also used for the file name
    :param length: int representing ngram size
    """
    nbfile = len(os.listdir("corpus/" + name))
    for i in range(nbfile):
        f = open("corpus/" + name + "/" + name + "_article" + str(i + 1) + ".txt", encoding="utf-8")
        tokens = tokenizeWord(f.read())
        ngrams = generateNGrams(tokens, length)
        probabilities = getAllNgramWithLastWordsProbability(ngrams, length)
        saveInJSONFile(name, probabilities, length)


def getAllNgramOfCategory(name, length):
    """
    function that save in a JSON file all ngram for a category
    :param name: string name of the category, also used for the file name
    :param length: int representing ngram size
    """
    nbfile = len(os.listdir("corpus/" + name))
    for i in range(nbfile):
        f = open("corpus/" + name + "/" + name + "_article" + str(i + 1) + ".txt", encoding="utf-8")
        tokens = tokenizeWord(f.read())
        ngrams = generateNGrams(tokens, length)
        saveInJSONFile(name + "ngrams", ngrams, length)


def getNgramWithLastWordsProbability(ngrams, probabilities):
    """
    function that create a list with first ngram without last word, and second a list of next word probability
    :param ngrams: list ngram without last word
    :param probabilities: list of word probabilities
    :return:
    """
    return [[" ".join(ngrams), probabilities]]


def getAllNgramWithLastWordsProbability(ngrams, ngramsize):
    """
    function that return all ngrams with their last word probability
    :param ngrams: list of ngrams
    :param ngramsize: int of ngrame size
    :return: list of all ngrams with their last word probability
    """
    res = []
    used = []
    for i in range(len(ngrams)):
        if ngrams[i] not in used:
            used += [ngrams[i]]
            n = ngrams[i]
            ngram = n.split()
            res += getNgramWithLastWordsProbability(ngram[:ngramsize - 1], sortProb(
                ngramAllWordProbability(convertNgramSentenceToNgramList(ngrams), ngram[:ngramsize - 1])))
    return res


def generateSentenceWithProbabilities(probabilities, begining, length, ngramsize, ngrams):
    """
    fonction that generate sentence using ngram probabilities
    :param probabilities: list of ngram and next word probabilities
    :param begining: list of a random ngram from all ngrams
    :param length: int that represent the number of word to generate
    :param ngramsize: int that represent size of ngrams
    :param ngrams: list of all ngrams
    :return: string of the generated sentence
    """
    beg = " ".join(begining)
    res = " ".join(begining)
    used = []
    end = False
    count = 0
    while end != True:
        shuffle(probabilities)
        for j in range(len(probabilities)):
            if probabilities[j][0] == beg:
                word = beg + probabilities[j][1][0][0]
                if word in used and len(probabilities[j][1]) > 1:
                    res += " " + probabilities[j][1][1][0]
                    split = beg.split()
                    split += [probabilities[j][1][1][0]]
                    del split[0]
                    beg = " ".join(split)
                    used += [word]
                elif res[-1] == ".":
                    beg2 = generateSentenceBegining(ngrams, ngramsize)
                    beg = " ".join(beg2)
                    res += beg
                    used += [beg]
                else:
                    res += " " + probabilities[j][1][0][0]
                    split = beg.split()
                    split += [probabilities[j][1][0][0]]
                    del split[0]
                    beg = " ".join(split)
                    used += [word]
        count += 1
        if len(res) >= length and res[-1] == ".":
            end = True
        elif count == 2 * length:
            res += "."
            end = True
    return res


def addCategoryWithTextFile(name, text):
    """
    function that add a category to the corpus
    :param name: string file name
    :param text: string file content
    """
    for i in range(2,6):
        getAllNgramOfNewCategoryAndProbabilities(name, text, i)



def launchMultipleCategory(categorie, longueur = 2, taillephrase = 10):
    """
    function to launch a sentence generation
    :param categorie: list of multiple category
    :param longueur: int size of the ngram
    :param taillephrase: int size of the sentence to be created
    :return: string of the sentence generated
    """
    prob = []
    ngrams = []
    for i in range(len(categorie)):
        prob += getFromJSONFile(categorie[i], longueur)
        ngrams += getFromJSONFile(categorie[i] + "ngrams", longueur)
    begining = generateSentenceBeginingWithoutLast(ngrams, longueur)
    return (generateSentenceWithProbabilities(prob, begining, taillephrase, longueur, ngrams))


def launch(categorie, longueur = 2, taillephrase = 10):
    """
    function to launch a sentence generation
    :param categorie: string of the asked category
    :param longueur: int size of the ngram
    :param taillephrase: int size of the sentence to be created
    :return: string of the sentence generated
    """
    prob = getFromJSONFile(categorie, longueur)
    ngrams = getFromJSONFile(categorie + "ngrams", longueur)
    begining = generateSentenceBeginingWithoutLast(ngrams, longueur)
    return generateSentenceWithProbabilities(prob, begining, taillephrase, longueur, ngrams)


if __name__ == '__main__':
    print(launch("culture", 3))
    print(launchMultipleCategory(["culture","politique"]))

